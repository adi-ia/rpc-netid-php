This folder contains resources for unit and integration tests. 

There should not be any keys are certs associated with the Middleware web service. Do not delete anything from this folder unless their associated
unit tests have been destroyed!

Certificates and keys that are safe to be in this folder and versioned:

* encryptedKey.*
* testClientCert.*
* middleware-ws-client-test-ca.cer
* *.wsdl
