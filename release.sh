#!/usr/bin/env bash
source ./.env

echo "Releasing version "$VERSION", the current state of branch "$BRANCH" on upstream"
echo Checking out branch $BRANCH...
git checkout $REMOTE/$BRANCH
echo Creating tag $VERSION...
git tag $VERSION
echo Pushing to remote $REMOTE...
git push $REMOTE $VERSION



