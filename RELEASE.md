# Release Process #

1. User has cloned the [primary repository](https://git.doit.wisc.edu/adi-ia/rpc-netid-php/) and has write access to it.
2. User has run `chmod +x release.sh`
3. User has modified the version variable in the **.env** file. If a different branch is to be released, that is also specified
in the .env file.
4. User runs the following command: `./release.sh`
5. The designated branch is checked out.
6. A tag is created with the name equal to the version number with no extra characters
7. The repository is pushed to origin with all tags. 

The end result is a new tag in the remote repository equal to the version number. Composer clients will now pick up on the new version number.
